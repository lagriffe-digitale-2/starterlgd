<?php
use StarterLGD\Autoloader;
use StarterLGD\Helpers\MediaHelper;
use StarterLGD\Hooks\WordPressHooks;
use StarterLGD\Hooks\acfHooks;

// Require Constants
require_once( __DIR__ . '/Config/constants.config.php');

// Require Vendor Autoload
require_once( __DIR__ . '/vendor/autoload.php' );

// Include Autoloader
require_once( __DIR__ . '/Tools/Autoloader.php' );

// Register Autoload
Autoloader::register();

// WordPress Hooks
new WordPressHooks();
// ACF Hooks
new acfHooks();


function get_formatted_filesize($bytes) {
    return MediaHelper::getFormattedFileSize($bytes);
}

function get_file_icon_class($type) {
    return MediaHelper::getFileIconClass($type);
}

/*
use StarterLGD\Helpers\MediaHelper;
// Include WP Generic hooks
//require(__DIR__ . '/Tools/wordPressGenericHooks.php');

function get_formatted_filesize($bytes) {
    return MediaHelper::getFormattedFileSize($bytes);
}

function get_file_icon_class($type) {
    return MediaHelper::getFileIconClass($type);
}
*/
