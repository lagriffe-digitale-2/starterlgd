/**
 * Diaporama managments based on Swiper JS library
 **/
import Swiper from 'swiper/dist/js/swiper.js';

export class LGD_Diaporama {
    constructor()
    {

    }

    // Init Swiper Diaporama
    initDiaporama(jsClass, uniqId)
    {
        let diaporamaSwiper = new Swiper('.' + jsClass, {
            direction       : 'horizontal',
            loop            : false,
            navigation      : {
                nextEl : '#' + uniqId + ' .swiper-button-next',
                prevEl : '#' + uniqId + ' .swiper-button-prev'
            }
        });
    }
}
