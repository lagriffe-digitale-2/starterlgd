/**
 * Component to generate Masonry Pictures wall (Content block) 
 */
export class LGD_PicturesWall {
    constructor()
    {
        //
    }

    // Init pictures wall
    initPicturesWall(jsClass, uniqId)
    {
        var $grid = $('.' + jsClass + '#' + uniqId).masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });

        $grid.imagesLoaded().progress( function( instance, image ) {
            $grid.masonry();
            var result = image.isLoaded ? 'loaded' : 'broken';
            $('#' + image.img.id).closest('.grid-item').addClass('loaded');
        });
    }
}
