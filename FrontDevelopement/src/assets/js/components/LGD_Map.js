/**
 * Component to generate a stylised GMaps
 * @param lat : Latitude
 * @param lng : Longitude
 * @param address : Address
 * @param mapStyle : Map Style (can be changed in javascript vars config)
 * @param websiteName : Website name (can be changed in javascript vars config)
 */
export class LGD_Map {
    constructor(lat, lng, address, mapStyle, websiteName, infowindowContent, infowindowOpen)
    {
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.mapStyle = (mapStyle !== '') ? mapStyle : '';
        this.websiteName = websiteName;
        this.infoWindowContent = infowindowContent;
        this.infoWindowOpen = infowindowOpen;
    }

    // Init Map
    initMap(mapContainerId)
    {
        // Map Style
        let styledMap = new google.maps.StyledMapType(
            this.mapStyle,{ name: this.websiteName }
        );
        // Map options
        let mapOptions = {
            zoom: 15,
            maxZoom : 22,
            center : new google.maps.LatLng(this.lat, this.lng),
            zoomControl: true,
            scaleControl: false,
            scrollwheel: false,
            disableDoubleClickZoom: true,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };
        // Init Google map Object with map options
        let map = new google.maps.Map(document.getElementById(mapContainerId), mapOptions);
        // Set map Style
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');
        // Add Infowindow with address
        let infoWindow = '';
        if (1 == this.infoWindowOpen) {
            // Formate address
            let infoWindowContent = '';
            if (this.infoWindowContent != '') {
                infoWindowContent = this.infoWindowContent;
            } else {
                infoWindowContent = this.formateAddress(this.websiteName, this.address);
            }
            infoWindow = new google.maps.InfoWindow({
                content: infoWindowContent,
                width : 340
            });
        }
        // Add marker
        let marker = new google.maps.Marker({
            position : new google.maps.LatLng(this.lat, this.lng),
            title : this.websiteName,
        });
        marker.setMap(map);
        // Open infowindow if not null
        if (infoWindow != '') {
            infoWindow.open(map,marker);
        }
    }

    // Formate address
    formateAddress(websiteName, address)
    {
        let infoWindowContent = '';
        infoWindowContent += '<strong>' + websiteName + '</strong><br/>';
        let splitAddress = address.split(',');
        if (splitAddress != '') {
            $.each(splitAddress, function(key, value) {
                infoWindowContent += value + '<br/>';
            });
        }
        return infoWindowContent;
    }
}
