import $ from 'jquery';
window.$ = $;

// Imports External libraries
require('@fancyapps/fancybox/dist/jquery.fancybox');

// Import LGD Components
import { LGD_Diaporama } from './components/LGD_Diaporama';
import { LGD_PicturesWall } from './components/LGD_PicturesWall';
import { LGD_Map } from './components/LGD_Map';

$(document).ready(function(){
    // Init diaporamas if exists
    if ($('.js-diaporama').length) {
        const lgd_diaporama = new LGD_Diaporama();
        $('.js-diaporama').each(function(){
            lgd_diaporama.initDiaporama('js-diaporama', $(this).attr('id'));
        });
    }

    // Pictures wall
    if ($('.js-pictures-wall').length) {
        const lgd_pictures_wall = new LGD_PicturesWall();
        $('.js-pictures-wall').each(function(){
            var attrId = $(this).attr('id');
            lgd_pictures_wall.initPicturesWall('js-pictures-wall', attrId);
        });
    }

    // Map Block
    if ($('.js-block-map').length) {
        $('.js-block-map').each(function(){
            let lgd_map = new LGD_Map(
                $(this).attr('data-lat'),
                $(this).attr('data-lng'),
                $(this).attr('data-address'),
                mapStyle,
                websiteName,
                $(this).attr('data-infowindow'),
                parseInt($(this).attr('data-infowindow-open'))
            );
            lgd_map.initMap($(this).attr('id'));
        });
    }

    // Accordion block
    $('.js-open-accordion').click(function(e){
        e.preventDefault();
        $(this).closest('.accordion').toggleClass('active');
        if ($(this).closest('.accordion').hasClass('active')) {
            let accordion_id = $(this).closest('.accordion').attr('id');
            let accordionOffset = $('#' + accordion_id).offset().top;
            $('html,body').animate({'scrollTop' : parseInt(accordionOffset) }, 600);
        }
    });

    // Onglet block
    $('.js-open-onglet').click(function(e){
        e.preventDefault();
        if (!$(this).closest('.onglet-nav-item').hasClass('active')) {
            $('.onglet-nav-item').removeClass('active');
            $(this).closest('.onglet-nav-item').addClass('active');
            let ongletId = $(this).attr('data-id');
            $('.onglets .onglet').removeClass('active');
            $('#' + ongletId ).addClass('active');
        }
    });
});
