<?php
namespace StarterLGD\Controllers;

use \Timber;

class FrontController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Render Template
     **/
    public function render()
    {
        // Get Template vars
        $context = Timber::get_context();
        $post = new \TimberPost();
        $context['post'] = $post;
        // Render Template
        $customTemplate = get_post_meta($post->ID, '_wp_page_template', TRUE);
        Timber::render(
            'pages/' . ((!empty($customTemplate) && 'default' != $customTemplate) ? $customTemplate : 'front.twig'),
            $context
        );
    }
}
