<?php
namespace StarterLGD\Controllers;

use \Timber;
use \Timber\Menu;
use \StarterLGD\Helpers\MediaHelper;

/**
 * Starter Class
 *
 * This class starts theme, add theme supports,  load i18n, load context and apply to Twig, register images types, CSS / JS Files and Custom post types
 **/
class Starter extends \TimberSite
{
    public function __construct()
    {
        // Define Timber Views folders
        Timber::$dirname = ['Templates', 'Views'];
        // Twig & Timber filters
        add_filter('timber_context', [$this, 'addToContext']);
        add_filter('get_twig', [$this, 'addToTwig']);
        // Add Menus to context
        add_filter('lgd_add_to_context', [$this, 'addMenusToContext'], 100, 1);
        // Construct parent
        parent::__construct();
    }

    /**
     * Add datas to general Timber context
     * @param array $context : General context
     * @return array $context : General context modified
     **/
    public function addToContext($context)
    {
        $context['site'] = $this;
        // Logo
        $mainLogo = get_field('main_logo', 'option');
        if (!empty($mainLogo)) {
            if ('image/svg+xml' == $mainLogo['mime_type']) {
                $uploadDir = wp_upload_dir();
                $pathFile = str_replace($uploadDir['baseurl'], $uploadDir['basedir'], $mainLogo['url']);
                $context['main_logo'] = MediaHelper::getSVG($pathFile);
            } else {
                $context['main_logo'] = '<img src="' . $mainLogo['sizes']['lgd_md_thumb'] . '" alt="' . get_bloginfo('name') . ' - ' . get_bloginfo('description') . '" />';
            }
        }
        // Social Networks
        $context['social_networks'] = [
            'facebook'      => get_field('facebook', 'option'),
            'twitter'       => get_field('twitter', 'option'),
            'instagram'     => get_field('instagram', 'option'),
            'linkedin'      => get_field('linkedin', 'option'),
            'pinterest'     => get_field('pinterest', 'option'),
        ];
        return apply_filters('lgd_add_to_context', $context);
    }

    /**
     * Add Twig support
     * @param object $twig : Twig object
     * @return object $twig
     **/
    public function addToTwig($twig)
    {
        $twig->addExtension( new \Twig_Extension_StringLoader() );
		return $twig;
    }


    /**
     * Add Starter menus to context
     * @param array $context
     * @return array $context
     **/
    public function addMenusToContext($context)
    {
        // Get registred menus
        $registredMenus = get_registered_nav_menus();
        if (!empty($registredMenus)) {
            foreach ($registredMenus as $menuId => $registredMenu) {
                // Add it to Timber context
                $menuIdentifier = str_replace('-', '_', $menuId);
                $context[$menuIdentifier] = new Menu($menuId);
            }
        }
        return $context;
    }
}
