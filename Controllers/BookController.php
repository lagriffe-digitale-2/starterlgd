<?php
namespace StarterLGD\Controllers;

use \Timber;

/**
 * Custom Post type "Book" Controller (Create for example)
 **/
class BookController extends BasicController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function render()
    {
        if (is_archive()) {
            print "Archive Book";
        } else {
            // Get Template vars
            $context = Timber::get_context();
            $post = new \TimberPost();
            // Render Template
            Timber::render('pages/single-book.twig', $context);
        }
    }
}
