<?php
use StarterLGD\Controllers\BasicController;

// Load Basic Controller to override WordPress Hierarchy
$basicController = new BasicController();

// Init Theme
$basicController->initTheme();

/*
use StarterLGD\Autoloader;
use StarterLGD\Controllers\BasicController;

// Require Constants
require_once( __DIR__ . '/Config/constants.config.php');

// Require Vendor Autoload
require_once( __DIR__ . '/vendor/autoload.php' );

// Include Autoloader
require_once( __DIR__ . '/Tools/Autoloader.php' );

// Register Autoload
Autoloader::register();

// Load Basic Controller to override WordPress Hierarchy
$basicController = new BasicController();

// Init Theme
$basicController->initTheme();
*/
