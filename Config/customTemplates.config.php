<?php
/**
 * Current post custom templates
 *
 *
 * Datas Structure
 *
 * [post_type_identifier] => [
 *    // List of templates
 *    template twig file => Name of template
 * ];
 *
 * You can use "lgd_custom_templates" filter to change custom Post types Templates
 *
 **/
return apply_filters('lgd_custom_templates',[
    'page'  => [
        'contact-page.twig' => 'Contact page',
    ]
]);
