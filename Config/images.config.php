<?php
return apply_filters('lgd_images_configuration', [
    [
        'image_id'  => 'lgd_sm_thumb',
        'width'     => '420',
        'height'    => '9999',
        'crop'      => TRUE
    ],
    [
        'image_id'  => 'lgd_md_thumb',
        'width'     => '860',
        'height'    => '9999',
        'crop'      => TRUE
    ],
    [
        'image_id'  => 'lgd_lrg_thumb',
        'width'     => '1280',
        'height'    => '9999',
        'crop'      => TRUE
    ],
    [
        'image_id'  => 'lgd_xlrg_thumb',
        'width'     => '1680',
        'height'    => '9999',
        'crop'      => TRUE
    ],
    [
        'image_id'  => 'lgd_rt_thumb',
        'width'     => '2150',
        'height'    => '9999',
        'crop'      => TRUE
    ],
    [
        'image_id'  => 'lgd_news_thumbnail',
        'width'     => '480',
        'height'    => '280',
        'crop'      => TRUE
    ]
]);
