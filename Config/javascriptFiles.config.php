<?php
return apply_filters('lgd_javascript_files', [
    /*
    [
        'handle'        => 'google-maps',
        'path'          => 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_API_KEY,
        'dependencies'  => '',
        'version'       => '',
        'in_footer'     => TRUE
    ],
    */
    [
        'handle'        => 'starter-lgd-main-js',
        'path'          => JS_PATH . '/app.js',
        'dependencies'  => ['jquery', 'masonry'],
        'version'       => '2.0.0',
        'in_footer'     => TRUE
    ]
]);
