<?php
$postTypes = [];

// Book Custom post type
$postTypes['book'] = [
    'singular_name' => 'book',
    'plural_name'   => 'books',
    'args'          => [
        'menu_icon' => 'dashicons-book-alt',
    ], // CPT args
    'taxonomies'    => [
        'genre' => [
            'singular_name' => 'genre',
            'plural_name'   => 'genres',
            'args'          => [] // Taxonomy args
        ]
    ]
];
return $postTypes;
