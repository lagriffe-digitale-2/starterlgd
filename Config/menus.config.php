<?php
return apply_filters('lgd_menus_list', [
    'main-menu'     => __('Main menu', 'starter_lgd'),
    'footer-menu'   => __('Footer menu', 'starter_lgd')
]);
