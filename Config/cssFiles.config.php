<?php
return apply_filters('lgd_css_files', [
    [
        'handle'        => 'starter-lgd-main-stylesheet',
        'path'          => CSS_PATH . '/app.css',
        'dependencies'  => [],
        'version'       => '2.0.0',
        'media'         => 'all'
    ],
    /*
    [
        'handle'        => 'fontawesome',
        'path'          => 'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
        'dependencies'  => [],
        'version'       => '5.8.2',
        'media'         => 'all'
    ]
    */
]);
